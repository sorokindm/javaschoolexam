package com.tsystems.javaschool.tasks.calculator;

import java.text.NumberFormat;
import java.util.ArrayList;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
    	
    	if (statement==null) return null;
    	
    	String result=null;
		// Find parenthesis with right/left method
    	while(statement.length()>2)
    	{
    		//Parentheses borders
			int right = -1;
			int left = -1;
			right = statement.indexOf(')');
			if (right == -1) 
			{
				left = statement.lastIndexOf('(');
				if (left == -1) 
				{
					//No parentheses found, calculate result
					result= calculateWithoutParentheses(statement);
					break;
				} 
				else 
				{
					//unmatched parentheses
					return null;
				}
			} 
			else 
			{
				left = statement.lastIndexOf('(', right);
				if (left == -1)
					//unmatched parentheses
					return null;
				if ((right - left) >= 1) 
				{
					//found pair of parentheses, calculate result and cut statement string
			    	String value=null;
					value = calculateWithoutParentheses(statement.substring(left + 1, right));
					if (value==null) return null;
					statement = statement.substring(0, left) + value
							+ statement.substring(right + 1, statement.length());
				} 
				else
					//nothing between parentheses
					return null; 
			}
		}
    	
    	//
    	if (result==null) return null;
    	
    	//Format numbers
		NumberFormat nf=NumberFormat.getNumberInstance();
		nf.setMaximumFractionDigits(4);
		result=nf.format(Double.parseDouble(result));
		result=result.replace(',', '.');
		return result;
    }
    
    private String calculateWithoutParentheses(String statement)
    {
		ArrayList<String> expressions=new ArrayList<String>();
    	for (int i=0;i<statement.length();i++)
    	{
    		char sign=statement.charAt(i);
    		
    		if(sign=='*'||sign=='/')
    		{
    			if (expressions.isEmpty()||i==statement.length())
    			{
    				//'*' or'/' sign in the beginning or in the end
    				return null;
    			}
    			else
    			{
    				char last=expressions.get(expressions.size()-1).charAt(expressions.get(expressions.size()-1).length()-1);
    				if (last=='*'||last=='/'||last=='+'||last=='-')
    				{
    					//[*,/,+,-] sign are near to * or /
    					return null;
    				}
    				expressions.add(""+sign);
    				continue;
    			}
    		}
    		
    		if (sign=='+'||sign=='-')
    		{
    			if (i==statement.length())
    				return null; //+ or - in the end of the string
    			if (!expressions.isEmpty())
    			{
    				char last=expressions.get(expressions.size()-1).charAt(expressions.get(expressions.size()-1).length()-1);
    				if (last=='+'||last=='-')
    				{
    					//+ or - are next to each other
    					return null;
    				}
    			}
				expressions.add(""+sign);
				continue;
    		}
    		
    		
    		if (expressions.isEmpty())
    		{
    			expressions.add(""+sign);
    			continue;
    		}
    		else
    		{
    			char last=expressions.get(expressions.size()-1).charAt(expressions.get(expressions.size()-1).length()-1);
				if (last=='*'||last=='/')
				{
					expressions.add(""+sign);
					continue;
				}
				if(last=='+'||last=='-')
				{
					if (expressions.size()>1)
					{
						char preLast=expressions.get(expressions.size()-2).charAt(expressions.get(expressions.size()-2).length()-1);
						if (preLast=='*'||preLast=='/')
						{
							expressions.set(expressions.size()-1, expressions.get(expressions.size()-1)+sign);
							continue;
						}
						else
						{
							expressions.add(""+sign);
							continue;
						}
					}
					else
					{
						expressions.set(expressions.size()-1, expressions.get(expressions.size()-1)+sign);
						continue;
					}
				}
				expressions.set(expressions.size()-1, expressions.get(expressions.size()-1)+sign);
    		}	
    	}
    	
    	
    	//Multiply and divide
    	for (int i=0;i<expressions.size();i++)
    	{
    		String value=expressions.get(i);
    		if (value.equals("*")||value.equals("/"))
    		{
    			try
    			{
        			double left=Double.parseDouble(expressions.get(i-1));
        			double right=Double.parseDouble(expressions.get(i+1));

        			double result=0;
        			if (value.equals("*"))
        				result=left*right;
        			else
        			{
        				if (right==0) 
        					return null;
        				result=left/right;
        			}
        			expressions.set(i-1, Double.toString(result));
        			expressions.remove(i+1);
        			expressions.remove(i);
    			}
    			catch(NumberFormatException x)
    			{
    				return null;
    			}
    			i=-1;
    		}
    	}
    	
    	//Add and substract
    	for (int i=0;i<expressions.size();i++)
    	{
    		String value=expressions.get(i);
    		if (value.equals("+")||value.equals("-"))
    		{
    			try
    			{
        			double left=Double.parseDouble(expressions.get(i-1));
        			double right=Double.parseDouble(expressions.get(i+1));

        			double result=0;
        			if (value.equals("+"))
        			{
        				result=left+right;
        			}
        			else
        			{
        				result=left-right;
        			}
        			expressions.set(i-1, Double.toString(result));
        			expressions.remove(i+1);
        			expressions.remove(i);
    			}
    			catch(NumberFormatException x)
    			{
    				return null;
    			}
    			i=-1;
    		}
    	}

    	return expressions.get(0);
    }

}
