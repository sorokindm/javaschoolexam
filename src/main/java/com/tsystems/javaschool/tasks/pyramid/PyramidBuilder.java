package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
    	
    	int height=0;
    	int width=0;
    	
		if (inputNumbers == null)
			throw new CannotBuildPyramidException();
		if (inputNumbers.size() < 3)
			throw new CannotBuildPyramidException();
		if (inputNumbers.contains(null))
			throw new CannotBuildPyramidException();

		double dims = (Math.sqrt(1 + 8 * inputNumbers.size()) - 1) / 2;
		height = (int) dims;
		// Check if pyramid is possible
		if (height != dims)
			throw new CannotBuildPyramidException();
		width = height + (height - 1);

		Collections.sort(inputNumbers);
    	
    	int[][] pyramid=new int[height][width];
    	
    	//Building Pyramid
		int count = 0;
		int placeInRow = 0;
		int skipInRow = 0;
		for (int i = 0; i < height; i++) {
			placeInRow = i + 1;
			skipInRow = (width - (placeInRow + placeInRow - 1)) / 2;
			
			for (int j = 0; j < width; j++) {
				if (skipInRow != 0) {
					pyramid[i][j] = 0;
					skipInRow--;
				} 
				else {
					if (placeInRow != 0) {
						pyramid[i][j] = inputNumbers.get(count);
						count++;
						placeInRow--;
						skipInRow++;
					}
				}
			}
		}
		
        return pyramid;
    }   
}
