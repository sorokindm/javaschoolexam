package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x==null||y==null) throw new IllegalArgumentException();
    	if (x.size()==0) return true;
    	if (y.size()==0) return false;
    	if (x.get(x.size()-1).equals(y.get(y.size()-1)))
    	{
    		x.remove(x.size()-1);
    		y.remove(y.size()-1);
    		return find (x,y);
    	}
    	
    	y.remove(y.size()-1);
    	return find(x,y);
    }
}
